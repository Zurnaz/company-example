# company-example

> Company example website

Single page website taking the advantage of static hosting.

Demo: https://zurnaz.gitlab.io/company-example

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```
