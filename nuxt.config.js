module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Example LTD',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: './cloud-line.svg' }
    ],
    script: []
  },
  /*
  ** Router base
  */
  router: {
    base: process.env.DEPLOY_ENV === 'STATIC' ? '/company-example/' : '/'
  },
  /*
  ** CSS and SASS imports
  */
  css: [
    // SCSS file in the project
    '@/assets/css/foundation-sites.scss'
    // Load a node module directly (here it's a SASS file)
    // have issues importing it directly
    // 'foundation-sites'
    // CSS file in the project
    // '@/assets/css/main.css',
  ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Customize the generated output folder
  */
  generate: {
    dir: 'public'
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, ctx) {
      if (ctx.dev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
